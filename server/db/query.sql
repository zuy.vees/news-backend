CREATE DATABASE platinum;

CREATE TABLE category (
    category_id BIGSERIAL PRIMARY KEY,
    is_delete BOOLEAN NOT NULL, 
    category_name VARCHAR(255) NOT NULL
);

CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  username VARCHAR(15) NOT NULL UNIQUE,
  password VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL UNIQUE,
  phone_number VARCHAR(255) NOT NULL,
  created_at TIMESTAMPTZ DEFAULT current_timestamp,
  updated_at TIMESTAMPTZ DEFAULT current_timestamp,
  deleted_at TIMESTAMPTZ DEFAULT current_timestamp,
  is_admin BOOLEAN DEFAULT false,
  is_delete BOOLEAN DEFAULT false,  
  category_user VARCHAR(12)
);


CREATE TABLE comment (
    comment_id BIGSERIAL PRIMARY KEY, 
    detail_comment TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL,
    modified_at TIMESTAMP NOT NULL,
    is_delete BOOLEAN NOT NULL, 
    news_id INT, 
    user_id INT, 
    CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES users(user_id)
);

CREATE TABLE news (
    news_id BIGSERIAL PRIMARY KEY, 
    title VARCHAR(255) NOT NULL, 
    description VARCHAR(255) NOT NULL,
    detail VARCHAR(255) NOT NULL, 
    image BYTEA NOT NULL,
    created_at TIMESTAMP NOT NULL,
    modified_at TIMESTAMP NOT NULL,
    recommendation BOOLEAN NOT NULL,
    view_number INT NOT NULL,
    title_desc VARCHAR(255) NOT NULL,
    is_delete BOOLEAN NOT NULL, 
    category_id INT, 
    user_id INT, 
    CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES users(user_id), 
    CONSTRAINT fk_category FOREIGN KEY(category_id) REFERENCES category(category_id) 
);

ALTER TABLE comment 
    ADD CONSTRAINT fk_news FOREIGN KEY(news_id) REFERENCES news(news_id);

ALTER TABLE news ALTER COLUMN detail TYPE TEXT;

INSERT INTO category (is_delete, category_name)
VALUES
    (false, 'Politik'),
    (false, 'Olahraga'),
    (false, 'Ekonomi'),
    (false, 'Entertainment'),
    (false, 'Entertainment'),
    (false, 'Budaya')
    (false, 'Mancanegara');

--truncate table category;
--SELECT setval('category_category_id_seq', 1, false);

INSERT INTO news  (title, description, detail, image, created_at, modified_at, recommendation, view_number, title_desc, is_delete, category_id, user_id)
VALUES
    ('Ini Sikap Sandiaga Uno Jika Tak Terpilih Jadi Cawapres Ganjar', 'Ketua Badan Pemenangan Pemilu (Bappilu) DPP Partai Persatuan Pembangunan (PPP), Sandiaga Salahuddin Uno, buka suara andai dirinya tidak terpilih menjadi cawapres Ganjar Pranowo.', 'Jakarta - Ketua Badan Pemenangan Pemilu (Bappilu) DPP Partai Persatuan Pembangunan (PPP), Sandiaga Salahuddin Uno, buka suara andai dirinya tidak terpilih menjadi cawapres Ganjar Pranowo. Bagaimana sikap Sandiaga?
"Ya kembali, kan, Innamal Amalu Binniyat, jadi semuanya dimulai dari niat, saya berniat bergabung di PPP ini bagamana bisa mendorong ekonomi hijau," ujar Sandi di Kompleks Parlemen DPR RI, Jakarta, Selasa (2/10/2023).

"Saya all out akan mendukung Pak Ganjar ini sudah diputuskan rapimnas 5 dan 6 kegiatan-kegiatan ini terus kita dorong," imbuh dia.

Sandi mengungkap PPP mendorongnya untuk melakukan pengabdian ke masyarakat. Dirinya pun juga menyebut sejumlah tokoh yang ditampilkan sekarang merupakan sosok terbaik bangsa.

"PPP mendorong saya untuk fokos pada pengabdian. Menurut saya siapa pun tokoh-tokoh yang tampil itu putra-putri terbaik bangsa dan ada di kewenangan pimpinan untuk penentuan langkah-langkah ke depan," ungkap dia.
Sandi juga menegaskan dirinya memilih berpolitik bukan untuk mencari jabatan atau kekuasaan. Dirinya mengatakan terjun ke politik untuk melakukan pengabdian.

"Saya dari pertama ke politik bukan untuk mencari jabatan atau kekuasaan. Saya alhamdulillah memberikan banyak karunia di dunia usaha. Saya ada di politik ini karena saya ingin pengabdian dan pengorbanan," kata Menparekraf itu.
', 'http://res.cloudinary.com/dcz5fjpsa/image/upload/v1696301793/rj7qckcmqr6wy86qdbk6.jpg', '2023-10-07 10:00:00', '2023-10-07 10:30:00', true, 1000, 'Politik', false, 1, 3),

INSERT INTO comment (detail_comment, created_at, modified_at, is_delete, news_id, user_id)
VALUES
    ('beritanya seru banget', '2023-10-07 10:00:00', '2023-10-07 10:00:00', false, 3, 3),
    ('semakin menarik aja', '2023-10-07 10:30:00', '2023-10-07 10:30:00', false, 4, 3);
